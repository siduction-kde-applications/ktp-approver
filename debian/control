Source: ktp-approver
Section: kde
Priority: optional
Maintainer: Debian KDE Extras Team <pkg-kde-extras@lists.alioth.debian.org>
Uploaders: Diane Trout <diane@ghic.org>,
           Michał Zając <quintasan@kubuntu.org>,
           Mark Purcell <msp@debian.org>,
           Maximiliano Curia <maxy@debian.org>
Build-Depends: cmake (>= 2.8.12~),
               debhelper (>= 9),
               extra-cmake-modules (>= 1.3.0~),
               kdelibs5-dev (>= 4:4.6),
               libkf5config-dev,
               libkf5dbusaddons-dev,
               libkf5i18n-dev,
               libkf5notifications-dev,
               libkf5service-dev,
               libtelepathy-qt4-dev (>= 0.9.0),
               libtelepathy-qt5-dev,
               pkg-kde-tools (>= 0.9),
               qtbase5-dev (>= 5.4)
Standards-Version: 3.9.5
Homepage: https://projects.kde.org/projects/extragear/network/telepathy/ktp-approver
Vcs-Git: git://anonscm.debian.org/pkg-kde/kde-extras/kde-telepathy/ktp-approver.git
Vcs-Browser: http://anonscm.debian.org/gitweb/?p=pkg-kde/kde-extras/kde-telepathy/ktp-approver.git

Package: kde-telepathy-approver
Architecture: any
Depends: kde-telepathy-data (>= ${source:Upstream-Version}),
         ${misc:Depends},
         ${shlibs:Depends}
Recommends: kde-telepathy
Description: KDED module for approving incoming conversations
 This package provides a KDED module that starts together with the KDE
 Plasma Desktop and shows a notification popup when someone from your
 contacts starts chatting with you.
 .
 This package is not meant to be used standalone. It is recommended
 to install the kde-telepathy metapackage instead.

Package: kde-telepathy-approver-dbg
Architecture: any
Section: debug
Priority: extra
Depends: kde-telepathy-approver (= ${binary:Version}), ${misc:Depends}
Description: KDED module for approving incoming conversations - debug symbols
 This package provides the debug symbols for the telepathy approver
 KDED module.
